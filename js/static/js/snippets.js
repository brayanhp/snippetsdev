$(function(){


    //Los parametros que tienen las funciones pueden ser reemplazadas por variables cuando se use.

    function validEmail(email)
    {
        var reg = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/;
        return reg.test(email);
    }

    var send = $('#send');
    send.on('click', function (e){
        var email = $('#email').val();
        e.preventDefault();
        if($.trim(email).length == 0){
            console.log('no hay caracteres');
        }else if(validEmail(email)){
            console.log('email valido');
        }else{
            console.log('email invalido');
        }
    });


    //Obtener urls con javascript

    //window.location.protocol = "http:"
    //window.location.host = "guineapig.pe"
    //window.location.pathname = "carpeta/index.html"


    //Obtener valores de variables en URLs

    function getValueInVariable(variable)
    {
        var query = window.location.search.substring(1);
        var vars = query.split("&");
        for (var i=0;i<vars.length;i++) {
            var pair = vars[i].split("=");
            if(pair[0] == variable){return pair[1];}
        }
        return(false);
    }

    //http://www.guineapig.pe/index.php?id=1&image=foto.jpg

    //getValueInVariable("id")

    //Contar cantidad de clicks

    $('#boton').data('counter', 0).click(function() {
            var counter = $(this).data('counter');
            $(this).data('counter', counter + 1);
            console.log($(this).data('counter'));
        });


    //Preloading de imagenes

    var cache = [];
    function preloadImages() {
        var args_len = arguments.length;
        for (var i = args_len; i--;) {
            var cacheImage = document.createElement('img');
            cacheImage.src = arguments[i];
            cache.push(cacheImage);
        }
    }
    preloadImages("imagen.gif", "/carpeta/imagen.png");



    //Carga y actualizacion por ajax load

    var wrapper = $("#wrapper");
    wrapper.load("backend.php");
    var refresh = setInterval(function() {
        wrapper.load("backend.php");
    }, 5000); // 5 segundos


    //Detectar movil(iPhone, iPad, iPod)

    function deviceAgent(){

        var deviceAgent = navigator.userAgent.toLowerCase();
        var agentID = deviceAgent.match(/(iphone|ipod|ipad)/);
        if (agentID) {
            console.log('tu usas ' + agentID);
        }
    }
    deviceAgent();

});